using UnityEngine;
using UnityEngine.AI;

namespace VeryRare.BehaviourTree 
{

    // The context is a shared object every node has access to.
    // Commonly used components and subsytems should be stored here
    // It will be somewhat specfic to your game exactly what to add here.
    // Feel free to extend this class 
    public class Context 
    {
        public GameObject gameObject;
        public Transform transform;
        public Animator animator;
        public NavMeshAgent agent;
        public CapsuleCollider capsuleCollider;
        public Renderer renderer;
        public AudioSource audioSource;
        
        public Context(GameObject go) 
        {
            CreateContext(go);
        }

        protected virtual void CreateContext(GameObject go)
        {
            this.gameObject     = go;
            transform           = go.transform;
            agent               = go.GetComponentInChildren<NavMeshAgent>();
            animator            = go.GetComponentInChildren<Animator>();
            if ( animator != null)
            {
                capsuleCollider     = animator.GetComponent<CapsuleCollider>();
                renderer            = animator.GetComponentInChildren<Renderer>();
            } else {
                capsuleCollider     = go.GetComponent<CapsuleCollider>();
                renderer            = go.GetComponent<Renderer>();
            }
            audioSource         = go.GetComponent<AudioSource>();
        }
    }
}
