namespace VeryRare.BehaviourTree 
{

    public interface IBehaviourTreeRunner
    {
        BehaviourTree GetTree();
    }

}
