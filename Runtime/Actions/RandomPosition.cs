using UnityEngine;

namespace VeryRare.BehaviourTree 
{

    [System.Serializable]
    public class RandomPosition : ActionNode
    {
        public bool relativeToCurrentPosition;

        public Vector2 min = Vector2.one * -10;
        public Vector2 max = Vector2.one * 10;

        protected override void OnStart() {
        }

        protected override void OnStop() {
        }

        protected override State OnUpdate() {
            blackboard.moveToPosition.x = Random.Range(min.x, max.x);
            blackboard.moveToPosition.z = Random.Range(min.y, max.y);
            blackboard.moveToPosition.y = context.agent.transform.position.y;
            if ( relativeToCurrentPosition)
            {
                blackboard.moveToPosition.x += context.agent.transform.position.x;
                blackboard.moveToPosition.z += context.agent.transform.position.z;
            }
            return State.Success;
        }
    }
}
