using UnityEngine;
using UnityEngine.AI;

namespace VeryRare.BehaviourTree 
{
    [System.Serializable]
    public class MoveToPosition : ActionNode
    {
        public float stoppingDistance = 0.1f;
        public float acceleration = 40.0f;
        public float tolerance = 0.2f;

        protected override void OnStart() 
        {
            var agent = context.agent;
            agent.stoppingDistance = stoppingDistance;
            agent.acceleration = acceleration;

            if (!context.agent.isOnNavMesh ) 
            {
                agent.Warp(context.transform.position);
                if (! context.agent.isOnNavMesh ) 
                    Debug.LogError("FAILED TO PUT AGENT ON NAVMESH " + context.gameObject.name, context.gameObject);
            }
            
            if (!context.agent.isStopped ) 
                context.agent.isStopped = false;

            if (agent.SetDestination(blackboard.moveToPosition))
                return;

            NavMeshPath path = new NavMeshPath();
            if (!NavMesh.CalculatePath(agent.nextPosition, blackboard.moveToPosition, new NavMeshQueryFilter(){agentTypeID = agent.agentTypeID, areaMask = -1 }, path)) 
                return;

            agent.SetPath(path);
        }

        protected override void OnStop() 
        {
        }

        protected override State OnUpdate() 
        {
            if (context.agent.pathPending) return State.Running;
    
            if (!context.agent.isOnNavMesh || !context.agent.hasPath || context.agent.isStopped )  
                return State.Failure;

            if (context.agent.remainingDistance < tolerance)  
                return State.Success;

            if ( context.agent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathInvalid ) 
                return State.Failure;

            return State.Running;
        }

        public override void OnDrawGizmos()
        {
            if ( blackboard == null) return;
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(context.transform.position, blackboard.moveToPosition);
            Gizmos.DrawSphere(blackboard.moveToPosition, 0.2f);
        }
    }
}
