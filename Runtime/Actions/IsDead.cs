
namespace VeryRare.BehaviourTree 
{

    [System.Serializable]
    public class IsDead : ActionNode
    {
        protected override void OnStart() {
        }

        protected override void OnStop() {
        }

        protected override State OnUpdate() {
            return blackboard.isDead ? State.Success : State.Failure;
        }
    }
    
}
