using UnityEngine;

namespace VeryRare.BehaviourTree 
{

    public class SubTreeActionNode : ActionNode
    {
        public BehaviourTree subTree;

        bool init = false;

        protected override void OnStart() {
            if ( init ) return;
            init = true;
            subTree.blackboard = blackboard;
            subTree.Bind(context);
        }

        protected override void OnStop() {
            // do subTree abort?
            if ( state == State.Running)
            {
                // state == State.Running -> called from abort
                subTree.rootNode.Abort();
            }
        }

        protected override State OnUpdate() {
            return subTree.Update();
        }

        public override void OnDrawGizmos() { 
            if (!subTree) {
                return;
            }
            BehaviourTree.Traverse(subTree.rootNode, (n) => {
                if (n.drawGizmos) {
                    n.OnDrawGizmos();
                }
            });
        }

        public override Node Clone()
        {
            var node = Instantiate(this);
            node.subTree = Instantiate(subTree);
            return node;
        }
    }

}
