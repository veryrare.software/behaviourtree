
namespace VeryRare.BehaviourTree 
{

    [System.Serializable]
    public class IsStaggered : ActionNode
    {
        protected override void OnStart() {
        }

        protected override void OnStop() {
        }

        public new Blackboard blackboard => base.blackboard as Blackboard;
       
        protected override State OnUpdate() {
            return blackboard.isStaggered ? State.Success : State.Failure;
        }
    }

}
