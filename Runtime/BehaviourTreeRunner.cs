using UnityEngine;

namespace VeryRare.BehaviourTree 
{

    public class BehaviourTreeRunner : MonoBehaviour, IBehaviourTreeRunner
    {
        public BehaviourTree tree;

        void Awake()
        {
            tree = tree.Clone();
            tree.blackboard = GetBehaviourTreeBlackboard(); 
            tree.Bind(CreateBehaviourTreeContext());
        }

        void Update() 
        {
            OnUpdate();
            if (tree != null) tree.Update();
        }

        protected virtual void OnUpdate()
        {
        }

        public virtual Context CreateBehaviourTreeContext() => new Context(gameObject);
        public virtual BaseBlackboard GetBehaviourTreeBlackboard() => new Blackboard();

        public BehaviourTree GetTree() => tree;

        private void OnDrawGizmosSelected() 
        {
            if (!tree) return;

            BehaviourTree.Traverse(tree.rootNode, (n) => {
                if (n.drawGizmos) {
                    n.OnDrawGizmos();
                }
            });
        }
    }
}
