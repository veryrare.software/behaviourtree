using UnityEngine;

namespace VeryRare.BehaviourTree 
{

    // This is the blackboard container shared between all nodes.
    // Use this to store temporary data that multiple nodes need read and write access to.
    // Add other properties here that make sense for your specific use case.
    
    [System.Serializable]
    public class BaseBlackboard
    {
        public Vector3  moveToPosition;
        public bool isDead;
    }
    
    [System.Serializable]
    public class Blackboard : BaseBlackboard
    {
        [Space]
        public Vector3  defaultPosition;  
        public Vector3  defaultAngles;               
        public Transform patrolPath;  
        public int patrolIndex = -1;             

        [Space]
        public float    alertProgress = 0f;         // current alert progress to goto combat

        [Space]
        public bool     targetVisible;              // from Vision
        public float    lastSeenTarget;             // from Vision
        public Vector3  lastTargetPosition;         // from Vision
        public float    lastTargetDistance;         // from Vision 
        public bool     underAttack;                // from Vision
        
        [Space]
        public bool     targetVisibleFirstFrame;                // from Vision
        public bool     targetNotVisibleFirstFrame;                // from Vision

        // public bool haveInteraction;
        // public Vector3 interactionPosition;

        public bool isStaggered;
    }
}