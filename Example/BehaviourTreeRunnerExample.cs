using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VeryRare.BehaviourTree
{
    // new Context class must inherit from Context
    class ExampleContext : Context 
    {
        public ExampleContext(GameObject go) : base(go)
        {
        }
    }
    
    // new Blackboard must inherit from Blackboard and should have  [System.Serializable]
    [System.Serializable]
    class ExampleBlackboard : Blackboard {}

    // for easier access to custom context and blackboard, all new actions inherit from ExampleActionNode instead of ActionNode
    abstract class ExampleActionNode : ActionNode 
    {
        public new ExampleContext context => base.context as ExampleContext;
        public new ExampleBlackboard blackboard => base.blackboard as ExampleBlackboard;
    }

    // <Context,Blackboard> can replaced with inherited classes
    public class BehaviourTreeRunnerExample : BehaviourTreeRunner
    {
        [SerializeField] ExampleBlackboard blackboard;

        public override Context CreateBehaviourTreeContext() => new ExampleContext(gameObject);
        public override Blackboard GetBehaviourTreeBlackboard() => blackboard;

        // start for init things, Awake is taken by BehaviourTreeRuner
        void Start()
        {

        }

        // update logic.. for updating blackboard vars etc..
        protected override void OnUpdate()
        {
            // blackboard.targetVisible = Time.time - context.vision.bestMemory.lastSeen < context.vision.lastSeenLimit;
        }

    }
}
