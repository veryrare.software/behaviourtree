using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using System;
using System.Linq;
using System.Reflection;
using VeryRare.BehaviourTree;
using Node = VeryRare.BehaviourTree.Node;

namespace TheKiwiCoder 
{
    
    public class BehaviourTreeView : GraphView {

        public Action<NodeView> OnNodeSelected;
        public new class UxmlFactory : UxmlFactory<BehaviourTreeView, GraphView.UxmlTraits> { }
        BehaviourTree tree;
        BehaviourTreeSettings settings;

        public BehaviourTreeEditor editorWindow;

        public struct ScriptTemplate {
            public TextAsset templateFile;
            public string defaultFileName;
            public string subFolder;
        }

        public ScriptTemplate[] scriptFileAssets = {
            
            new ScriptTemplate{ templateFile=BehaviourTreeSettings.GetOrCreateSettings().scriptTemplateActionNode, defaultFileName="NewActionNode.cs", subFolder="Actions" },
            new ScriptTemplate{ templateFile=BehaviourTreeSettings.GetOrCreateSettings().scriptTemplateCompositeNode, defaultFileName="NewCompositeNode.cs", subFolder="Composites" },
            new ScriptTemplate{ templateFile=BehaviourTreeSettings.GetOrCreateSettings().scriptTemplateDecoratorNode, defaultFileName="NewDecoratorNode.cs", subFolder="Decorators" },
        };

        private BehaviourTreeSearchWindow _searchWindow;

        public BehaviourTreeView() {
            settings = BehaviourTreeSettings.GetOrCreateSettings();
            Insert(0, new GridBackground());

            this.AddManipulator(new ContentZoomer());
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new DoubleClickSelection());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());
            this.AddManipulator(new DragAndDropManipulator(this, onDragAndDroppedObject));

            var styleSheet = settings.behaviourTreeStyle;
            styleSheets.Add(styleSheet);

            Undo.undoRedoPerformed += OnUndoRedo;

            // add search window
            _searchWindow = ScriptableObject.CreateInstance<BehaviourTreeSearchWindow>();
            _searchWindow.Configure(editorWindow);
            nodeCreationRequest = context => OpenSearchWindow(context.screenMousePosition, (t, mousePos)=>CreateNode(t as Type, mouseToGraphPosition( mousePos - editorWindow.position.position)) ); 

            serializeGraphElements = SerializeGraphElementsImpl;
            unserializeAndPaste = UnserializeAndPasteImpl;
        }

        public void OpenSearchWindow(Vector2 mousePosition, Action<object, Vector2> onSelect)
        {
            _searchWindow.onSelect = onSelect;
            SearchWindow.Open<BehaviourTreeSearchWindow>(new SearchWindowContext(mousePosition), _searchWindow);
        }

        public Vector2 mouseToGraphPosition(Vector2 mousePosition)
        {
            var res = editorWindow.rootVisualElement.ChangeCoordinatesTo(editorWindow.rootVisualElement.parent, mousePosition);
            res = contentViewContainer.WorldToLocal(res);
            return res;
        }

        public Vector2 graphToMousePosition(Vector2 graphPos)
        {
            var res = contentViewContainer.LocalToWorld(graphPos);
            res = editorWindow.rootVisualElement.parent.ChangeCoordinatesTo(editorWindow.rootVisualElement, res);
            return res;
        }

        void onDragAndDroppedObject(UnityEngine.Object obj, Vector2 mousePosition)
        {
            if (obj is MonoScript mono && mono.GetClass().IsSubclassOf(typeof(Node)))
            {
                CreateNode(mono.GetClass(), mouseToGraphPosition(mousePosition) );
            }
        }

        private void OnUndoRedo() {
            PopulateView(tree);
            AssetDatabase.SaveAssets();
        }

        public NodeView FindNodeView(Node node) {
            return GetNodeByGuid(node.guid) as NodeView;
        }

        internal void PopulateView(BehaviourTree tree) {
            this.tree = tree;

            graphViewChanged -= OnGraphViewChanged;
            DeleteElements(graphElements.ToList());
            graphViewChanged += OnGraphViewChanged;

            if (tree.rootNode == null) {
                tree.rootNode = tree.CreateNode(typeof(RootNode)) as RootNode;
                EditorUtility.SetDirty(tree);
                AssetDatabase.SaveAssets();
            }

            if ( tree.nodes == null ) return;
            // Creates node view
            tree.nodes.ForEach(n => {
                if ( n != null)
                    CreateNodeView(n);
            });
            // Create edges
            tree.nodes.ForEach(n => {
                var children = BehaviourTree.GetChildren(n);
                children.ForEach(c => {
                    NodeView parentView = FindNodeView(n);
                    NodeView childView = FindNodeView(c);

                    Edge edge = parentView.output.ConnectTo(childView.input);
                    AddElement(edge);
                });
            });
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter) {
            return ports.ToList().Where(endPort =>
            endPort.direction != startPort.direction &&
            endPort.node != startPort.node).ToList();
        }

        private GraphViewChange OnGraphViewChanged(GraphViewChange graphViewChange) {
            if (graphViewChange.elementsToRemove != null) {
                graphViewChange.elementsToRemove.ForEach(elem => {
                    NodeView nodeView = elem as NodeView;
                    if (nodeView != null) {
                        tree.DeleteNode(nodeView.node);
                    }

                    Edge edge = elem as Edge;
                    if (edge != null) {
                        NodeView parentView = edge.output.node as NodeView;
                        NodeView childView = edge.input.node as NodeView;
                        tree.RemoveChild(parentView.node, childView.node);
                    }
                });
            }

            if (graphViewChange.edgesToCreate != null) {
                graphViewChange.edgesToCreate.ForEach(edge => {
                    NodeView parentView = edge.output.node as NodeView;
                    NodeView childView = edge.input.node as NodeView;
                    tree.AddChild(parentView.node, childView.node);
                });
            }

            nodes.ForEach((n) => {
                NodeView view = n as NodeView;
                view.SortChildren();
            });

            return graphViewChange;
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt) {

            base.BuildContextualMenu(evt);
            evt.menu.AppendSeparator();
            // New script functions
            evt.menu.AppendAction($"Create Script.../New Action Node", (a) => CreateNewScript(scriptFileAssets[0]));
            evt.menu.AppendAction($"Create Script.../New Composite Node", (a) => CreateNewScript(scriptFileAssets[1]));
            evt.menu.AppendAction($"Create Script.../New Decorator Node", (a) => CreateNewScript(scriptFileAssets[2]));
            evt.menu.AppendSeparator();
            // return;
            // Vector2 nodePosition = this.ChangeCoordinatesTo(contentViewContainer, evt.localMousePosition);
            // {

            //     var types = TypeCache.GetTypesDerivedFrom<ActionNode>();
            //     foreach (var type in types) {
            //         evt.menu.AppendAction($"[Action]/{type.Name}", (a) => CreateNode(type, nodePosition));
            //     }
            // }

            // {
            //     var types = TypeCache.GetTypesDerivedFrom<CompositeNode>();
            //     foreach (var type in types) {
            //         evt.menu.AppendAction($"[Composite]/{type.Name}", (a) => CreateNode(type, nodePosition));
            //     }
            // }

            // {
            //     var types = TypeCache.GetTypesDerivedFrom<DecoratorNode>();
            //     foreach (var type in types) {
            //         evt.menu.AppendAction($"[Decorator]/{type.Name}", (a) => CreateNode(type, nodePosition));
            //     }
            // }
        }

        void SelectFolder(string path) {
            // https://forum.unity.com/threads/selecting-a-folder-in-the-project-via-button-in-editor-window.355357/
            // Check the path has no '/' at the end, if it does remove it,
            // Obviously in this example it doesn't but it might
            // if your getting the path some other way.

            if (path[path.Length - 1] == '/')
                path = path.Substring(0, path.Length - 1);

            // Load object
            UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));

            // Select the object in the project folder
            Selection.activeObject = obj;

            // Also flash the folder yellow to highlight it
            EditorGUIUtility.PingObject(obj);
        }

        void CreateNewScript(ScriptTemplate template) {
            SelectFolder($"{settings.newNodeBasePath}/{template.subFolder}");
            var templatePath = AssetDatabase.GetAssetPath(template.templateFile);
            ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, template.defaultFileName);
        }

        NodeView CreateNode(System.Type type, Vector2 position, bool save = true) {
            Node node = tree.CreateNode(type, save);
            node.position = position;
            return CreateNodeView(node);
        }

        NodeView CreateNodeView(Node node) {
            NodeView nodeView = new NodeView(OnDropOutside, node);
            nodeView.OnNodeSelected = OnNodeSelected;
            AddElement(nodeView);
            return nodeView;
        }

        private void OnDropOutside(Edge edge, Port port, Vector2 graphPosition)
        {
            
            OpenSearchWindow(editorWindow.position.position+graphPosition, (t, mousePos)=>{
                var newNodewView = CreateNode(t as Type, mouseToGraphPosition( mousePos - editorWindow.position.position) );
                // if is from output port
                if ( port.direction == Direction.Output)
                {
                    // connect to input of new node
                    var e = newNodewView.input.ConnectTo(port);
                    if ( e != null) 
                    {
                        tree.AddChild((port.node as NodeView).node, newNodewView.node);
                        AddElement(e);
                    }
                } else {
                    var e = newNodewView.output.ConnectTo(port);
                    if ( e != null) 
                    {
                        tree.AddChild(newNodewView.node, (port.node as NodeView).node);
                        AddElement(e);
                    }
                }
            });            
        }

        public void UpdateNodeStates() {
            nodes.ForEach(n => {
                NodeView view = n as NodeView;
                view.UpdateState();
            });
        }

        protected override bool canCopySelection => selection.OfType<NodeView>().Any();
        protected override bool canDuplicateSelection    => canCopySelection;
        protected override bool canPaste                 => true;

        [System.Serializable]
        public class CopyPasteData
        {
            public List<string> nodesData;
            public List<string> nodeTypes;
            public List<Vector2> nodePositions;
            // edges
            public List<int> edgeInputPort;
            public List<int> edgeOutputPort;
        }

        private void UnserializeAndPasteImpl(string operationName, string data)
        {
            Undo.RegisterCompleteObjectUndo(tree, operationName);
            var copy = JsonUtility.FromJson<CopyPasteData>(data);
            List<NodeView> newNodes = new List<NodeView>();
            List<ISelectable> newNodesSelection = new List<ISelectable>();
            this.selection = newNodesSelection;

            for(int i = 0; i < copy.nodesData.Count; i++)
            {
                var t = GetType(copy.nodeTypes[i]);
                var nodeView = CreateNode(t, copy.nodePositions[i] + new Vector2(20f,20f), false);
                var guid = nodeView.node.guid;
                JsonUtility.FromJsonOverwrite(copy.nodesData[i], nodeView.node);
                nodeView.node.guid = guid;
                if ( nodeView.node is CompositeNode cn) cn.children = new List<Node>();
                if ( nodeView.node is DecoratorNode dn) dn.child = null;
                newNodes.Add(nodeView);
                newNodesSelection.Add(nodeView);
            }
            // find indices in new nodes
            for(int i = 0; i < copy.edgeOutputPort.Count; i++)
            {
                var nodeOut = newNodes[copy.edgeOutputPort[i]];
                var nodeIn = newNodes[copy.edgeInputPort[i]];
                var e = nodeIn.input.ConnectTo(nodeOut.output);
                if ( e != null) 
                {
                    tree.AddChild(nodeOut.node, nodeIn.node);
                    AddElement(e);
                }
            }
            AssetDatabase.SaveAssets();
            
            this.selection = newNodesSelection;
        }

        private string SerializeGraphElementsImpl(IEnumerable<GraphElement> elements)
        {            
            var copy = new CopyPasteData();
            copy.nodesData = new List<string>(); 
            copy.nodeTypes = new List<string>(); 
            copy.nodePositions = new List<Vector2>(); 
            copy.edgeInputPort = new List<int>(); 
            copy.edgeOutputPort = new List<int>(); 
            var nodeViews = new List<NodeView>(); 
            foreach(var el in elements)
            {
                if ( el is NodeView n)
                {
                    copy.nodesData.Add(JsonUtility.ToJson(n.node));
                    copy.nodeTypes.Add(n.node.GetType().ToString());
                    copy.nodePositions.Add(n.node.position);
                    nodeViews.Add(n);
                }
            }
            foreach(var el in elements)
            {
                if ( el is Edge edge)
                {
                    var inIdx = nodeViews.IndexOf(edge.input.node as NodeView);
                    var outIdx = nodeViews.IndexOf(edge.output.node as NodeView);
                    if ( inIdx != -1 && outIdx != -1)
                    {
                        copy.edgeInputPort.Add(inIdx);
                        copy.edgeOutputPort.Add(outIdx);
                    }
                }
            }
            var json = JsonUtility.ToJson(copy, true);
            return json;
        }

        public static Type GetType( string TypeName )
        {
            // Try Type.GetType() first. This will work with types defined
            // by the Mono runtime, in the same assembly as the caller, etc.
            var type = Type.GetType( TypeName );
            // If it worked, then we're done here
            if( type != null )
                return type;
            // If the TypeName is a full name, then we can try loading the defining assembly directly
            // if( TypeName.Contains( "." ) )
            // {
            //     // Get the name of the assembly (Assumption is that we are using 
            //     // fully-qualified type names)
            //     var assemblyName = TypeName.Substring( 0, TypeName.IndexOf( '.' ) );
            //     // Attempt to load the indicated Assembly
            //     var assembly = Assembly.Load( assemblyName );
            //     if( assembly == null )
            //         return null;
            //     // Ask that assembly to return the proper Type
            //     type = assembly.GetType( TypeName );
            //     if( type != null )
            //         return type;
        
            // }
            // If we still haven't found the proper type, we can enumerate all of the 
            // loaded assemblies and see if any of them define the type
            var currentAssembly = Assembly.GetExecutingAssembly();
            var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
            foreach( var assemblyName in referencedAssemblies )
            {
                // Load the referenced assembly
                var assembly = Assembly.Load( assemblyName );
                if( assembly != null )
                {
                    // See if that assembly defines the named type
                    type = assembly.GetType( TypeName );
                    if( type != null )
                        return type;
                }
            }
            // The type just couldn't be found...
            return null;
        }


    }

}