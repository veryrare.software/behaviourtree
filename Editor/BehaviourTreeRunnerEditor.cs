namespace VeryRare.BehaviourTree 
{
    #if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(BehaviourTreeRunner), true)]
    public class BehaviourTreeRunnerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var sc = target as BehaviourTreeRunner;
            if (GUILayout.Button("Open Editor")) 
            {
                var wnd = BehaviourTreeEditor.OpenWindow();
                wnd.SelectTree(sc.GetTree());
            }

            if (GUILayout.Button("Create Path")) 
            {
                var bBase = sc.GetBehaviourTreeBlackboard();
                if ( bBase is Blackboard b)
                {
                    b.patrolPath = new GameObject(sc.gameObject.name + "_path").transform;
                    b.patrolPath = sc.transform.parent;
                    b.patrolPath.position = sc.transform.position;
                    b.patrolPath.rotation = sc.transform.rotation;
                    b.patrolPath.SetSiblingIndex(sc.transform.GetSiblingIndex()+1);
                    Selection.activeGameObject = b.patrolPath.gameObject;
                }
            }
            base.OnInspectorGUI();
        }
    }
    #endif
}
