using System;
using System.Collections.Generic;
using System.Reflection;
using TheKiwiCoder;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;


namespace VeryRare.BehaviourTree 
{

    public class BehaviourTreeSearchWindow : ScriptableObject, ISearchWindowProvider
    {
        BehaviourTreeView graphView;
        Texture2D _indentationIcon;
        BehaviourTreeEditor editorWindow;

        public Action<object, Vector2> onSelect;

        public void Configure(BehaviourTreeEditor editorWindow)
        {
            this.editorWindow = editorWindow;
            
            //Transparent 1px indentation icon as a hack
            _indentationIcon = new Texture2D(1,1);
            _indentationIcon.SetPixel(0,0,new Color(0,0,0,0));
            _indentationIcon.Apply();
        }

        List<SearchTreeEntry> ISearchWindowProvider.CreateSearchTree(SearchWindowContext context)
        {
            var tree = new List<SearchTreeEntry>();
            tree.Add(new SearchTreeGroupEntry(new GUIContent("Create Node"),0));

            tree.Add(new SearchTreeGroupEntry(new GUIContent("Action"),     1));
            AddTypes<ActionNode>(tree);

            tree.Add(new SearchTreeGroupEntry(new GUIContent("Composite"), 1));
            AddTypes<CompositeNode>(tree);

            tree.Add(new SearchTreeGroupEntry(new GUIContent("Decorator"), 1));
            AddTypes<DecoratorNode>(tree);

            return tree;
        }

        void AddTypes<T>(List<SearchTreeEntry> l)
        {
            var types = TypeCache.GetTypesDerivedFrom<T>();
            // foreach(var t in types)
            // {
            //     if ( t.GetTypeInfo().IsAbstract ) 
            //     {
            //         // l.Add(new SearchTreeGroupEntry(new GUIContent("__"+t.ToString()+"__"), 2));
            //         foreach(var t2 in types)
            //         {
            //             if ( t2.BaseType == t)
            //             {
            //                 var s = t2.ToString().Split('.');
            //                 l.Add(new SearchTreeEntry(new GUIContent(s[s.Length-1], _indentationIcon)) { level = 2,  userData = t2 } );
            //             }
            //         }
            //     }
            // }

            foreach(var t in types)
            {
                if ( !t.GetTypeInfo().IsAbstract ) 
                {
                    // var n = (t.BaseType == typeof(T)  ? "" : t.BaseType.ToString()+" : " ) + t;
                    // if (t.IsSubclassOf(typeof(T)))
                    {
                        var s = t.ToString().Split('.');
                        l.Add(new SearchTreeEntry(new GUIContent(s[s.Length-1], _indentationIcon)) { level = 2,  userData = t } );
                    }
                } 
            }
        }

        public bool OnSelectEntry(SearchTreeEntry SearchTreeEntry, SearchWindowContext context)
        {
            if ( SearchTreeEntry.userData != null)
            {
                onSelect(SearchTreeEntry.userData as System.Type, context.screenMousePosition);
                return true;
            }
            return false;
        }

    }

}
